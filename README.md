# My Firefox profile

When I install Firefox on a new machine, this Firefox profile is set up.

## Features and settings

- DuckDuckGo is the default search engine.
- [AdNauseam](https://adnauseam.io/) is installed and enabled for ad-blocking.
- VimVixen installed for vim bindings.
- Cleans up the UI.

## Removes unwanted:

- Push/web notifications
- Websites affecting the clipboard
- Geolocation
- Prefetching (Firefox interacting with websites you haven't clicked)
- Pocket
- Telemetry

## Installation

This repo should go in `~/.mozilla/firefox/`. You should `cp` or `rsync` it into that directory, it will overwrite the `profiles.ini` file. To retrieve old profiles,  modify `profiles.ini` to set them as the `Path` in `[Profile0]`.
